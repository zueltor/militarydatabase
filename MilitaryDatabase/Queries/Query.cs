﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public abstract class Query
    {
        public string Name { get; set; }
        public List<string> subqueries { get; set; }
        public List<Parameter> parameters;
        public int index { get; set; }
        public abstract void reset();
        public string sql()
        {
            if (index != -1)
            {
                return subqueries[index];
            }
            else
            {
                return null;
            };

        }
    }
}
