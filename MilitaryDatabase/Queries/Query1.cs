﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query1 :Query
    {
        public Query1()
        {
            this.Name = "Получить перечень всех частей военного округа (и их командиров)";
            this.subqueries = new List<string>
            {
                "SELECT NAME as MILITARY_UNIT, Personnel.FULL_NAME AS LEADER FROM Subdivisions JOIN Personnel ON LEADER_ID=Personnel.ID WHERE TYPE_ID = 4",
                "WITH DIVISION_SUBDIVISIONS AS (SELECT *FROM Subdivisions   START WITH ID = :subdivision_id   CONNECT BY PRIOR ID = ID_HIGHER ) SELECT NAME AS MILITARY_UNIT, Personnel.FULL_NAME AS LEADER FROM DIVISION_SUBDIVISIONS JOIN Personnel ON LEADER_ID = Personnel.ID WHERE TYPE_ID = 4"
            };
            this.parameters = new List<Parameter>
            {
                new Parameter("...указанной армии, дивизии, корпуса","subdivision_id",ParameterType.REFERENCE,"ID","NAME","SELECT ID, NAME FROM SUBDIVISIONS WHERE TYPE_ID > 4")

            };
        }

        public override void reset()
        {
            this.index = 0;
        }
    }
}
