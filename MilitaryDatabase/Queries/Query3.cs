﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query3 : Query
    {
        public Query3()
        {
            this.Name = "Получить данные по рядовому и сержантскому составу в целом";
            this.subqueries = new List<string>
            {
                "SELECT FULL_NAME, Personnel_Ranks.NAME AS RANK FROM Personnel       JOIN Personnel_Ranks USING (RANK_ID) WHERE RANK_ID BETWEEN 1 and 4 ORDER BY RANK_ID, FULL_NAME",
                "SELECT FULL_NAME, Personnel_Ranks.NAME AS RANK FROM Personnel         JOIN Personnel_Ranks USING (RANK_ID) WHERE RANK_ID = :personnel_rank ORDER BY RANK_ID, FULL_NAME",
                "WITH RELATED_SUBDIVISIONS AS (   SELECT ID   FROM Subdivisions   START WITH ID = :subdivision_id   CONNECT BY PRIOR ID = ID_HIGHER),    RELATED_PERSONNELS AS (        SELECT LEADER_ID AS ID        FROM Subdivisions        WHERE Subdivisions.ID IN (SELECT * FROM RELATED_SUBDIVISIONS)        UNION        SELECT ID        FROM Soldiers        WHERE SQUAD_ID IN (SELECT *FROM RELATED_SUBDIVISIONS)    ) SELECT FULL_NAME, Personnel_Ranks.NAME AS RANK FROM Personnel JOIN Personnel_Ranks USING (RANK_ID) WHERE (ID IN (SELECT * FROM RELATED_PERSONNELS)) AND RANK_ID = :personnel_rank"
            };
            this.parameters = new List<Parameter>
            {
                new Parameter("...и с учетом указанного звания всех частей военного округа","personnel_rank",ParameterType.REFERENCE,"RANK_ID","NAME","SELECT * FROM PERSONNEL_RANKS WHERE RANK_ID BETWEEN 1 AND 4"),
                new Parameter("...отдельной армии, дивизии, корпуса, военной части","subdivision_id",ParameterType.REFERENCE,"ID","NAME","SELECT ID, NAME FROM SUBDIVISIONS WHERE TYPE_ID >= 4")
            };
        }
        public override void reset()
        {
            this.index = 0;
        }
    }
}
