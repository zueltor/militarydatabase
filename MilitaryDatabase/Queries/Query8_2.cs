﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query8_2 : Query
    {
        public Query8_2()
        {
            this.Name = "Получить перечень военных частей, в которых нет указанной боевой техники";
            this.subqueries = new List<string>
            {
               "SELECT Subdivisions.NAME FROM Subdivisions WHERE TYPE_ID = 4  AND ID NOT IN (SELECT MU_ID                 FROM MU_Vehicle                 WHERE ID = 3                   AND AMOUNT > 0)"
            };
            this.parameters = new List<Parameter>
            {
                new Parameter("Боевая техника ","vehicle_id",ParameterType.REFERENCE,"ID","NAME","SELECT * FROM VEHICLE")

            };
        }
        public override void reset()
        {
            this.index = -1;
        }
    }
}
