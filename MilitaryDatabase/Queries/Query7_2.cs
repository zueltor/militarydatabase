﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query7_2 : Query
    {
        public Query7_2()
        {
            this.Name = "Получить перечень дислокаций, где дислоцировано более одной военной части";
            this.subqueries = new List<string>
            {
                "SELECT CITY FROM (SELECT DISLOCATION_ID, COUNT(MU_ID) AS COUNT      FROM Constructions      GROUP BY DISLOCATION_ID)         JOIN Dislocations ON DISLOCATION_ID = Dislocations.ID WHERE COUNT > 1"
            };
            this.parameters = new List<Parameter>();
        }
        public override void reset()
        {
            this.index = 0;
        }
    }
}
