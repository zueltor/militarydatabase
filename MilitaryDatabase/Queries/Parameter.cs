﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Parameter
    {
        public Parameter(string label, string name, ParameterType type, string valueName=null, string displayName = null, string parameterSQL=null)
        {
            Label = label;
            Name = name;
            Type = type;
            DisplayName = displayName;
            ValueName = valueName;
            ParameterSQL = parameterSQL;
            Enabled = false;
        }

        public string Label { get; set; }
        public string Name { get; set; }
        public ParameterType Type { get; set; }
        public string DisplayName { get; set; }
        public string ValueName { get; set; }
        public string ParameterSQL { get; set; }
        public bool Enabled { get; set; }

    }
}
