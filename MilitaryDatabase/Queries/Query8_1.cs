﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query8_1 : Query
    {
        public Query8_1()
        {
            this.Name = "Получить перечень военных частей, в которых число единиц указанного вида боевой техники больше 5";
            this.subqueries = new List<string>
            {
               "SELECT Subdivisions.NAME FROM MU_Vehicle         JOIN Subdivisions ON MU_ID = Subdivisions.ID WHERE AMOUNT > 5  AND MU_Vehicle.VEHICLE_ID = :vehicle_id"
            };
            this.parameters = new List<Parameter>
            {
                new Parameter("Боевая техника ","vehicle_id",ParameterType.REFERENCE,"ID","NAME","SELECT * FROM VEHICLE")

            };
        }
        public override void reset()
        {
            this.index = -1;
        }
    }
}
