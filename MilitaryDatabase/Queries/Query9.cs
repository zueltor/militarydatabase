﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query9 : Query
    {
        public Query9()
        {
            this.Name = "Получить данные о наличии вооружения в целом";
            this.subqueries = new List<string>
            {
                "SELECT Subdivisions.NAME, Armament.NAME, AMOUNT FROM MU_Armament       JOIN Armament ON MU_Armament.ARMAMENT_ID = Armament.ID       JOIN Subdivisions ON MU_ID = Subdivisions.ID ORDER BY Subdivisions.NAME, AMOUNT DESC",
                "SELECT Subdivisions.NAME, Armament.NAME, AMOUNT FROM MU_Armament       JOIN Armament ON MU_Armament.ARMAMENT_ID = Armament.ID       JOIN Subdivisions ON MU_ID = Subdivisions.ID WHERE Armament.ID = :armament_id ORDER BY Subdivisions.NAME, AMOUNT DESC",
                "WITH ALL_MU AS (SELECT ID, NAME                FROM (SELECT ID, NAME, TYPE_ID                      FROM Subdivisions                      START WITH ID = :subdivision_id                      CONNECT BY PRIOR id = ID_HIGHER)                WHERE TYPE_ID = 4) SELECT ALL_MU.NAME, Armament.NAME, AMOUNT FROM MU_Armament         JOIN Armament ON MU_Armament.ARMAMENT_ID = Armament.ID         JOIN ALL_MU ON MU_ID = ALL_MU.ID WHERE Armament.ID = :armament_id"

            };
            this.parameters = new List<Parameter>
            {
                new Parameter("...и с учетом указанной категории или вида во всех частях военного округа","armament_id",ParameterType.REFERENCE,"ID","NAME","SELECT * FROM ARMAMENT"),
                new Parameter("...в отдельной армии, дивизии, корпусе, военной части","subdivision_id",ParameterType.REFERENCE,"ID","NAME","SELECT ID, NAME FROM SUBDIVISIONS WHERE TYPE_ID >= 4")
            };
        }
        public override void reset()
        {
            this.index = 0;
        }
    }
}
