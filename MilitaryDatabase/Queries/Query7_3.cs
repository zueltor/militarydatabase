﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query7_3 : Query
    {
        public Query7_3()
        {
            this.Name = "Получить перечень незанятых сооружений";
            this.subqueries = new List<string>
            {
                "SELECT NAME FROM Constructions WHERE MU_ID IS NULL"
            };
            this.parameters = new List<Parameter>();
        }
        public override void reset()
        {
            this.index = 0;
        }
    }
}
