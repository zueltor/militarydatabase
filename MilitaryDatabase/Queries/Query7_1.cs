﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query7_1 : Query
    {
        public Query7_1()
        {
            this.Name = "Получить перечень сооружений указанной военной части";
            this.subqueries = new List<string>
            {
                "SELECT NAME FROM Constructions WHERE MU_ID = :subdivision_id"
            };
            this.parameters = new List<Parameter>
            {
                new Parameter("Военная часть ","subdivision_id",ParameterType.REFERENCE,"ID","NAME","SELECT ID, NAME FROM SUBDIVISIONS WHERE TYPE_ID = 4")

            };
        }
        public override void reset()
        {
            this.index = -1;
        }
    }
}
