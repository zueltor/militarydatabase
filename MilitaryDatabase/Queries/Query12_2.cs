﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query12_2 : Query
    {
        public Query12_2()
        {
            this.Name = "Получить перечень военных частей, в которых нет указанного вооружения";
            this.subqueries = new List<string>
            {
                "SELECT Subdivisions.NAME FROM Subdivisions WHERE TYPE_ID = 4  AND ID NOT IN (SELECT MU_ID                 FROM MU_Armament                 WHERE ID = :armament_id                   AND AMOUNT > 0)"
            };
            this.parameters = new List<Parameter>
            {
                new Parameter("Вооружение ","armament_id",ParameterType.REFERENCE,"ID","NAME","SELECT ID, NAME FROM Armament")
            };
        }
        public override void reset()
        {
            this.index = -1;
        }
    }
}
