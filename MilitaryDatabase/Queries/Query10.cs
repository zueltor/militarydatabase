﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query10 : Query
    {
        public Query10()
        {
            this.Name = "Получить перечень военных специальностей, по которым в округе более пяти специалистов";
            this.subqueries = new List<string>
            {
                "WITH TOTALS AS (SELECT COUNT(*) AS TOTAL, SPECIALIZATION_ID                FROM Personnel                         JOIN Personnel_Specialization ON id = PERSONNEL_ID                         JOIN Specializations ON SPECIALIZATION_ID = Specializations.ID                GROUP BY SPECIALIZATION_ID) SELECT Specializations.NAME FROM TOTALS         JOIN Specializations ON SPECIALIZATION_ID = Specializations.ID WHERE TOTAL > 5",
                "WITH RELATED_SUBDIVISIONS AS (    SELECT ID    FROM Subdivisions    START WITH ID = :subdivision_id    CONNECT BY PRIOR id = ID_HIGHER),     RELATED_PERSONNELS AS (         SELECT LEADER_ID AS ID         FROM Subdivisions         WHERE Subdivisions.ID IN (SELECT * FROM RELATED_SUBDIVISIONS)         UNION         SELECT ID         FROM Soldiers         WHERE SQUAD_ID IN (SELECT *FROM RELATED_SUBDIVISIONS)     ),     TOTALS AS (SELECT COUNT(*) AS TOTAL, SPECIALIZATION_ID                FROM RELATED_PERSONNELS                         JOIN Personnel_Specialization ON ID = PERSONNEL_ID                         JOIN Specializations ON SPECIALIZATION_ID = Specializations.ID                GROUP BY SPECIALIZATION_ID) SELECT Specializations.NAME FROM TOTALS         JOIN Specializations ON SPECIALIZATION_ID = Specializations.ID WHERE TOTAL > 5"

            };
            this.parameters = new List<Parameter>
            {
                new Parameter("...в отдельной армии, дивизии, корпусе, военной части","subdivision_id",ParameterType.REFERENCE,"ID","NAME","SELECT ID, NAME FROM SUBDIVISIONS WHERE TYPE_ID > 4")

            };
        }

        public override void reset()
        {
            this.index = 0;
        }
    }
}
