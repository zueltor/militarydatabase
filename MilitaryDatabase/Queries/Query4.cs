﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query4 : Query
    {
        public Query4()
        {
            this.Name = "Получить цепочку подчиненности снизу доверху для указанного военнослужащего";
            this.subqueries = new List<string>
            {
                "SELECT Personnel.FULL_NAME, Personnel_Ranks.NAME, Personnel.ID, LEVEL AS RANK FROM Subdivisions         JOIN Personnel ON Subdivisions.LEADER_ID = Personnel.ID         JOIN Personnel_Ranks USING (RANK_ID) START WITH LEADER_ID = :P_ID        OR Subdivisions.ID = (SELECT SQUAD_ID FROM Soldiers WHERE ID = :P_ID) CONNECT BY PRIOR ID_HIGHER = Subdivisions.ID UNION SELECT FULL_NAME, Personnel_Ranks.NAME, Personnel.ID, (0) FROM Soldiers         JOIN Personnel ON Soldiers.ID = Personnel.ID         JOIN Personnel_Ranks ON Personnel.RANK_ID = Personnel_Ranks.RANK_ID WHERE Soldiers.ID = :P_ID ORDER BY RANK"
            };
            this.parameters = new List<Parameter>
            {
                new Parameter("Военнослужащий","P_ID",ParameterType.REFERENCE,"ID","FULL_NAME","SELECT ID, FULL_NAME FROM PERSONNEL")

            };
        }
        public override void reset()
        {
            this.index = -1;
        }
    }
}
