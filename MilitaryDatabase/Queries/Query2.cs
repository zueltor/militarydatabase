﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query2 : Query
    {
        public Query2()
        {
            this.Name = "Получить данные по офицерскому составу в целом";
            this.subqueries = new List<string>
            {
                "SELECT FULL_NAME, Personnel_Ranks.NAME AS RANK FROM Personnel        JOIN Personnel_Ranks USING (RANK_ID) WHERE RANK_ID > 4 ORDER BY RANK_ID, FULL_NAME",
                "SELECT FULL_NAME, Personnel_Ranks.NAME AS RANK FROM Personnel        JOIN Personnel_Ranks USING(RANK_ID) WHERE RANK_ID = :officer_rank ORDER BY RANK_ID, FULL_NAME ",
                "SELECT FULL_NAME, Personnel_Ranks.NAME AS RANK, Subdivisions.NAME AS SUBDIVISION FROM Personnel        JOIN Personnel_Ranks USING(RANK_ID)        JOIN Subdivisions ON Personnel.ID = Subdivisions.LEADER_ID WHERE RANK_ID = :officer_rank AND Subdivisions.ID = :subdivision_id ORDER BY RANK_ID, FULL_NAME "
            };
            this.parameters = new List<Parameter>
            {
                new Parameter("...и по офицерскому составу указанного звания всех частей военного округа","officer_rank",ParameterType.REFERENCE,"RANK_ID","NAME","SELECT RANK_ID, NAME FROM PERSONNEL_RANKS WHERE RANK_ID >= 5"),
                new Parameter("...отдельной армии, дивизии, корпуса, военной части","subdivision_id",ParameterType.REFERENCE,"ID","NAME","SELECT ID, NAME FROM SUBDIVISIONS WHERE TYPE_ID >= 4")
            };
        }
        public override void reset()
        {
            this.index = 0;
        }
    }
}
