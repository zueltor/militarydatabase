﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query6 : Query
    {
        public Query6()
        {
            this.Name = "Получить данные о наличии боевой технике в целом";
            this.subqueries = new List<string>
            {
                "SELECT Subdivisions.NAME, Vehicle.NAME, AMOUNT FROM MU_Vehicle        JOIN Vehicle ON MU_Vehicle.VEHICLE_ID = Vehicle.ID        JOIN Subdivisions ON MU_ID = Subdivisions.ID ORDER BY Subdivisions.NAME, AMOUNT DESC",
                "SELECT Subdivisions.NAME, Vehicle.NAME, AMOUNT FROM MU_Vehicle        JOIN Vehicle ON MU_Vehicle.VEHICLE_ID = Vehicle.ID        JOIN Subdivisions ON MU_ID = Subdivisions.ID WHERE Vehicle.ID = :vehicle_id ORDER BY Subdivisions.NAME, AMOUNT DESC",
                "WITH ALL_MU AS (SELECT ID, NAME                FROM (SELECT ID, NAME, TYPE_ID                      FROM Subdivisions                      START WITH ID = :subdivision_id                      CONNECT BY PRIOR id = ID_HIGHER)                WHERE TYPE_ID = 4) SELECT ALL_MU.NAME, Vehicle.NAME, AMOUNT FROM MU_Vehicle         JOIN Vehicle ON MU_Vehicle.VEHICLE_ID = Vehicle.ID         JOIN ALL_MU ON MU_ID = ALL_MU.ID WHERE Vehicle.ID = :vehicle_id"
            };
            this.parameters = new List<Parameter>
            {
                new Parameter(" ...и с учетом указанной категории или вида во всех частях военного округа","vehicle_id",ParameterType.REFERENCE,"ID","NAME","SELECT * FROM VEHICLE"),
                new Parameter("...в отдельной армии, дивизии, корпусе, военной части","subdivision_id",ParameterType.REFERENCE,"ID","NAME","SELECT ID, NAME FROM SUBDIVISIONS WHERE TYPE_ID >= 4")
            };
        }
        public override void reset()
        {
            this.index = 0;
        }
    }
}
