﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query13_2 : Query
    {
        public Query13_2()
        {
            this.Name = "Получить данные об армии, дивизии, корпусе, в которые входит меньше всего военных частей";
            this.subqueries = new List<string>
            {
                "SELECT NAME, MU_COUNT FROM (SELECT NAME, MU_COUNT, RANK() OVER (ORDER BY MU_COUNT ASC) AS RANK      FROM TABLE ( get_subdiv_mu_counts() )               JOIN Subdivisions ON SUBDIVISION_ID = Subdivisions.ID) WHERE RANK = 1"
            };
            this.parameters = new List<Parameter>();
        }
        public override void reset()
        {
            this.index = 0;
        }
    }
}
