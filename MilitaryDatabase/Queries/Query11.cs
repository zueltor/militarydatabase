﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query11 : Query
    {
        public Query11()
        {
            this.Name = "Получить перечень военнослужащих указанной специальности в округе";
            this.subqueries = new List<string>
            {
                "SELECT Personnel.FULL_NAME, Specializations.NAME FROM Personnel         JOIN Personnel_Specialization ON PERSONNEL_ID = Personnel.ID         JOIN Specializations ON SPECIALIZATION_ID = Specializations.ID WHERE SPECIALIZATION_ID = :specialization_id",
                "WITH RELATED_SUBDIVISIONS AS (    SELECT ID    FROM Subdivisions    START WITH ID = :subdivision_id    CONNECT BY PRIOR id = ID_HIGHER),     RELATED_PERSONNELS AS (         SELECT LEADER_ID AS ID         FROM Subdivisions         WHERE Subdivisions.ID IN (SELECT * FROM RELATED_SUBDIVISIONS)         UNION         SELECT ID         FROM Soldiers         WHERE SQUAD_ID IN (SELECT *FROM RELATED_SUBDIVISIONS)     ) SELECT Personnel.FULL_NAME, Specializations.NAME FROM RELATED_PERSONNELS         JOIN Personnel ON RELATED_PERSONNELS.ID = Personnel.ID         JOIN Personnel_Specialization ON PERSONNEL_ID = Personnel.ID         JOIN Specializations ON SPECIALIZATION_ID = Specializations.ID WHERE SPECIALIZATION_ID = :specialization_id"
            };
            this.parameters = new List<Parameter>
            {
                new Parameter("Специальность","specialization_id",ParameterType.REFERENCE,"ID","NAME","SELECT * FROM SPECIALIZATIONS"),
                new Parameter("...в отдельной армии, дивизии, корпусе, военной части, в указанном подразделении некоторой военной части","subdivision_id",ParameterType.REFERENCE,"ID","NAME","SELECT ID, NAME FROM SUBDIVISIONS")
            };
        }

        public override void reset()
        {
            this.index = -1;
        }
    }
}
