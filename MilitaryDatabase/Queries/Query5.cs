﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query5 : Query
    {
        public Query5()
        {
            this.Name = "Получить перечень мест дислокации всех частей военного округа";
            this.subqueries = new List<string>
            {
                "SELECT DISTINCT CITY FROM Constructions         JOIN Dislocations ON DISLOCATION_ID = Dislocations.ID WHERE MU_ID IS NOT NULL ORDER BY CITY",
                "WITH ALL_MU AS (SELECT ID                FROM Subdivisions                START WITH ID = :subdivision_id                CONNECT BY PRIOR ID = ID_HIGHER) SELECT DISTINCT CITY FROM Constructions         JOIN Dislocations ON DISLOCATION_ID = Dislocations.ID         JOIN Subdivisions ON Constructions.MU_ID = Subdivisions.ID WHERE Subdivisions.ID IN (SELECT * FROM ALL_MU)"

            };
            this.parameters = new List<Parameter>
            {
                new Parameter("...отдельной армии, дивизии, корпуса, военной части","subdivision_id",ParameterType.REFERENCE,"ID","NAME","SELECT ID, NAME FROM SUBDIVISIONS WHERE TYPE_ID >= 4")
            };
        }
        public override void reset()
        {
            this.index = 0;
        }
    }
}
