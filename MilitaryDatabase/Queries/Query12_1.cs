﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Queries
{
    public class Query12_1 : Query
    {
        public Query12_1()
        {
            this.Name = "Получить перечень военных частей, в которых число единиц указанного вида вооружения больше 10";
            this.subqueries = new List<string>
            {
                "SELECT Subdivisions.NAME FROM MU_Armament         JOIN Subdivisions ON MU_ID = Subdivisions.ID WHERE AMOUNT > 10  AND MU_Armament.ARMAMENT_ID = :armament_id "
            };
            this.parameters = new List<Parameter>
            {
                new Parameter("Вооружение ","armament_id",ParameterType.REFERENCE,"ID","NAME","SELECT ID, NAME FROM Armament")

            };
        }
        public override void reset()
        {
            this.index = -1;
        }
    }
}
