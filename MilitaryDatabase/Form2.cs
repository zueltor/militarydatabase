﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MilitaryDatabase.Tables;
using Oracle.ManagedDataAccess.Client;

namespace MilitaryDatabase
{
    public partial class Form2 : Form
    {
        Form1 parentForm;
        Table selectedTable;
        DataGridViewRow selectedRow;
        string connectionString;
        bool editForm;
        public Form2(Form1 parentForm, string connectionString, Table selectedTable, DataGridViewRow selectedRow)
        {
            
            this.editForm = true;
            this.parentForm = parentForm;
            this.connectionString = connectionString;
            this.selectedTable = selectedTable;
            this.selectedRow = selectedRow;


            //Console.WriteLine(selectedRow.Cells[0].Value);
            InitializeComponent();
            this.Text = "Изменение записи";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;

        }

        public Form2(Form1 parentForm, string connectionString, Table selectedTable)
        {
            
            this.editForm = false;
            this.parentForm = parentForm;
            this.connectionString = connectionString;
            this.selectedTable = selectedTable;


            //Console.WriteLine(selectedRow.Cells[0].Value);
            InitializeComponent();
            this.Name = "Добавление записи";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;

        }


        private void UpdateEditPanel()
        {
            int i = 0;
            //this.tableLayoutPanel1.Controls.Clear();

            foreach (var field in selectedTable.Fields)
            {
                if (field.special && editForm)
                {
                    continue;
                }
                Control control;
                CheckBox checkBox = new CheckBox();
                checkBox.Checked = true;
                if (!editForm)
                {
                    checkBox.Enabled = false;
                }
                Label l1 = new Label
                {
                    Text = field.Name,
                    AutoSize = false,
                    TextAlign = ContentAlignment.MiddleLeft,
                    Dock = DockStyle.Fill
                };
                this.tableLayoutPanel1.Controls.Add(checkBox, 0, i);
                this.tableLayoutPanel1.Controls.Add(l1, 1, i);
                if (field.type == FieldType.REFERENCE)
                {
                    DataTable comboTable = new DataTable();
                    string query = string.Format("SELECT {0}, {1} FROM {2}", field.ReferenceName, field.DisplayName, field.ReferenceTable);
                    if (field.Where != null)
                    {
                        query += " WHERE " + field.Where;
                    }
                    OracleDataAdapter comboAdapter = new OracleDataAdapter(query, connectionString);
                    comboAdapter.Fill(comboTable);
                    object[] empty = { 0, "" };
                    comboTable.Rows.Add(empty);

                    ComboBox comboBox = new ComboBox
                    {
                        DataSource = comboTable,
                        DisplayMember = field.DisplayName,
                        ValueMember = field.ReferenceName,
                        DropDownStyle = ComboBoxStyle.DropDownList,
                        Dock=DockStyle.Fill
                };
                    control = comboBox;
                    this.tableLayoutPanel1.Controls.Add(control, 2, i);

                    if (editForm)
                    {
                        comboBox.SelectedValue = selectedRow.Cells[i].Value;
                    }

                    if (!field.IsEditable && editForm)
                    {
                        //checkBox.Enabled = false;
                        comboBox.Enabled = false;
                    }
                    //this.dataGridView2.Columns.Add(combo);
                }
                else if(field.type==FieldType.TEXT)
                {

                    TextBox textBox = new TextBox();
                    textBox.Dock = DockStyle.Fill;
                    control = textBox;
                    this.tableLayoutPanel1.Controls.Add(control, 2, i);
                    if (!field.IsEditable && editForm)
                    {
                        //checkBox.Enabled = false;
                        textBox.Enabled = false;
                    }
                    if (editForm)
                    {
                        textBox.Text = selectedRow.Cells[i].Value.ToString();
                    }

                    //this.tableLayoutPanel1.Controls.Add(textBox, 2, i);
                }else if (field.type == FieldType.DATE)
                {
                    DateTimePicker dateTimePicker = new DateTimePicker()
                    {
                        Format = DateTimePickerFormat.Custom,
                        CustomFormat = "d-MM-yyyy",
                        Dock = DockStyle.Fill
                };
                    control = dateTimePicker;
                    this.tableLayoutPanel1.Controls.Add(control, 2, i);
                    if (!field.IsEditable && editForm)
                    {
                        dateTimePicker.Enabled = false;
                    }
                    if (editForm)
                    {
                        
                        dateTimePicker.Value= DateTime.Parse(selectedRow.Cells[i].Value.ToString());
                    }
                }
                else
                {
                    NumericUpDown numeric = new NumericUpDown();
                    numeric.Dock = DockStyle.Fill;
                    numeric.Maximum = int.MaxValue;
                    numeric.Minimum = 0;
                    control = numeric;
                    this.tableLayoutPanel1.Controls.Add(control, 2, i);
                    if (!field.IsEditable && editForm)
                    {
                        numeric.Enabled = false;
                    }
                    if (editForm)
                    {
                        //string parseValue = selectedRow.Cells[i].Value.ToString();
                        //decimal value = (parseValue == string.Empty) ? 0 : decimal.Parse(selectedRow.Cells[i].Value.ToString());
                        numeric.Value = decimal.Parse(selectedRow.Cells[i].Value.ToString());
                    }
                }
                checkBox.CheckedChanged += (sender, e) => { checkChanged(sender, control); };




                i++;
            }
        }

        static void checkChanged(object sender, Control control)
        {
            CheckBox checkBox = (CheckBox)sender;
            if (checkBox.Checked)
            {
                control.Enabled = true;
            }
            else
            {
                control.Enabled = false;
            }
        }


        private void UpdateQuery()
        {
            int i = 0;
            string query = "UPDATE " + selectedTable.DbName + "\nSET ";
            string where = "\nWHERE ";
            int updateParameters = 0;
            int keyParameters = 0;
            foreach (DataGridViewCell cell in selectedRow.Cells)
            {
                CheckBox checkBox = (CheckBox)this.tableLayoutPanel1.Controls[i * 3];
                string oldValue = cell.Value.ToString(); ;
                string newValue;
                if (checkBox.Checked)
                {
                    Control control = this.tableLayoutPanel1.Controls[i * 3 + 2];
                    if (selectedTable.Fields[i].ReferenceName != null)
                    {
                        ComboBox comboBox = (ComboBox)control;
                        if (comboBox.SelectedValue == null)
                        {
                            newValue = "";
                        }
                        else
                        {
                            newValue = comboBox.SelectedValue.ToString();
                        }

                    }
                    else
                    {
                        newValue = control.Text;
                    }
                    if (selectedTable.Fields[i].IsNullable && "0".Equals(newValue))
                    {
                        newValue = "";
                    }

                    if (!newValue.Equals(oldValue))
                    {
                        if (updateParameters != 0)
                        {
                            query += ", ";
                        }
                        updateParameters++;
                        query += selectedTable.Fields[i].DbName + "='" + newValue + "'";
                    }
                }

                if (selectedTable.Fields[i].IsKey)
                {
                    if (keyParameters != 0)
                    {
                        where += " AND ";
                    }
                    keyParameters++;
                    where += selectedTable.Fields[i].DbName + "=" + oldValue;
                }
                i++;
            }
            query += where;

            if (updateParameters != 0)
            {
                try { 
                executeQuery(query);
                }
                catch (OracleException ex)
                {
                    foreach (OracleError error in ex.Errors)
                    {
                        MessageBox.Show(error.Message, "Error");
                    }
                    return;
                }

            }
            this.Close();
        }

        private void insertQuery()
        {
            string query = "INSERT INTO " + selectedTable.DbName + " VALUES(";
            int valuesCount = 0;
            string value;
            for (int i = 0; i < selectedTable.Fields.Count; i++)
            {
                Control control = this.tableLayoutPanel1.Controls[i * 3 + 2];
                if (selectedTable.Fields[i].ReferenceName != null)
                {
                    ComboBox comboBox = (ComboBox)control;
                    if (comboBox.SelectedValue == null)
                    {
                        value = "";
                    }
                    else
                    {
                        value = comboBox.SelectedValue.ToString();
                    }

                }
                else
                {
                    value = control.Text;
                }
                if (selectedTable.Fields[i].IsNullable && "0".Equals(value))
                {
                    value = "";
                }
                if (valuesCount > 0)
                {
                    query += ",";
                }
                valuesCount++;
                query += "'" + value + "'";
            }
            query += ")";
            try
            {
                executeQuery(query);
            }
            catch (OracleException ex)
            {
                foreach (OracleError error in ex.Errors)
                {
                    MessageBox.Show(error.Message, "Error");
                }
                return;
            }
            this.Close();

        }

        private void executeQuery(string query)
        {
            Console.WriteLine(query);
            using (OracleConnection conn = new OracleConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();
                OracleCommand command = conn.CreateCommand();
                command.CommandText = query;
               
                    int a = command.ExecuteNonQuery();
                    Console.WriteLine("Affected " + a + " rows");
               
               
            }
        }

        private void specialInsertQuery()
        {
            string query = "BEGIN insert_personnel(";
            int valuesCount = 0;
            string value;
            for (int i = 0; i < selectedTable.Fields.Count; i++)
            {
                if (selectedTable.Fields[i].IsKey)
                {
                    continue;
                }
                Control control = this.tableLayoutPanel1.Controls[i * 3 + 2];
                if (selectedTable.Fields[i].ReferenceName != null)
                {
                    ComboBox comboBox = (ComboBox)control;
                    if (comboBox.SelectedValue == null)
                    {
                        value = "";
                    }
                    else
                    {
                        value = comboBox.SelectedValue.ToString();
                    }

                }
                else
                {
                    value = control.Text;
                }
                if (selectedTable.Fields[i].IsNullable && "0".Equals(value))
                {
                    value = "";
                }
                if (valuesCount > 0)
                {
                    query += ",";
                }
                valuesCount++;
                query += "'" + value + "'";
            }
            query += "); END;";
            try
            {
                executeQuery(query);
            }
            catch (OracleException ex)
            {
                foreach (OracleError error in ex.Errors)
                {
                    MessageBox.Show(error.Message, "Error");
                }
                return;
            }
            this.Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (editForm)
            {
                UpdateQuery();
            }
            else if (selectedTable.SpecialInsert)
            {
                specialInsertQuery();
            }
            else { 
                insertQuery(); }

        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.parentForm.UpdateData();
            this.parentForm.Enabled = true;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            UpdateEditPanel();
        }
    }
}
