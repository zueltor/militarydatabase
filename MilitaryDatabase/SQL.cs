﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace MilitaryDatabase
{
    class SQL
    {
        private string connectionString;

        public SQL(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void executeQuery(string query)
        {
            Console.WriteLine(query);
            using (OracleConnection conn = new OracleConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();
                OracleCommand command = conn.CreateCommand();
                command.CommandText = query;

                int a = command.ExecuteNonQuery();
                Console.WriteLine("Affected " + a + " rows");


            }
        }
    }
}
