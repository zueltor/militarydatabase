﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Armament : Table
    {
        public Armament()
        {
            this.DbName = "Armament";
            this.Name = "Вооружение";
            this.Fields = new List<Field> {
                new Field("ID", "Идентификатор вооружения",FieldType.NUMBER, false, true),
                new Field("NAME","Название вооружения",FieldType.TEXT)
        };

        }
    }
}
