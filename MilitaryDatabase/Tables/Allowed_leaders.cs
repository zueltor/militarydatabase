﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Allowed_leaders : Table
    {
        public Allowed_leaders()
        {
            this.DbName = "Allowed_leaders";
            this.Name = "Возможные командиры";
            this.isInsertable = false;
            this.isRemoveable = false;
            this.isEditable = false;
            this.Fields = new List<Field> {
                new Field("TYPE_ID", "Тип подразделения",FieldType.REFERENCE, false, true,"Subdivision_Types","TYPE_ID","NAME"),
                new Field("RANK_ID", "Звание",FieldType.REFERENCE, false, true,"Personnel_Ranks","RANK_ID","NAME")
        };

        }
    }
}
