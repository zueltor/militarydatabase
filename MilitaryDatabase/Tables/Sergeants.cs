﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Sergeants : Table
    {
        public Sergeants()
        {
            this.DbName = "Sergeants";
            this.Name = "Сержанты";
            this.Fields = new List<Field> {
                new Field("ID", "ФИО",FieldType.REFERENCE, false, true,"Personnel","ID","FULL_NAME"),
                new Field("HIRE_DATE", "Дата найма", FieldType.DATE),
        };

        }
    }
}
