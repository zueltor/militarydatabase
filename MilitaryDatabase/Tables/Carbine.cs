﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Carbine : Table
    {
        public Carbine()
        {
            this.DbName = "Carbine";
            this.Name = "Карабины";
            this.Fields = new List<Field> {
                new Field("ID", "Карабин",FieldType.REFERENCE, false, true,"Armament","ID","NAME"),
                new Field("MAX_RANGE","Максимальная дальность",FieldType.NUMBER)
        };

        }
    }
}
