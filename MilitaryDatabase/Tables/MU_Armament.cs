﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class MU_Armament : Table
    {
        public MU_Armament()
        {
            this.DbName = "MU_Armament";
            this.Name = "Вооружение военных частей";
            this.Fields = new List<Field> {
                new Field("ARMAMENT_ID", "Вооружение",FieldType.REFERENCE, false, true,"Armament","ID","NAME"),
                new Field("MU_ID", "Военная часть",FieldType.REFERENCE, false, true,"Subdivisions","ID","NAME",false,"TYPE_ID = 4"),
                new Field("AMOUNT","Количество",FieldType.NUMBER)
        };

        }
    }
}
