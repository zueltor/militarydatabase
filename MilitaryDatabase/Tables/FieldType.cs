﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    public enum FieldType
    {
        NUMBER,
        TEXT,
        DATE,
        REFERENCE
    }
}
