﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Subdivisions : Table
    {
        public Subdivisions()
        {
            this.DbName = "Subdivisions";
            this.Name = "Подразделения";
            this.Fields = new List<Field> {
                               new Field("ID", "Идентификатор",FieldType.NUMBER, false, true),
                new Field("ID_HIGHER", "Вышестоящее подразделение", FieldType.REFERENCE,true, false, "Subdivisions", "ID", "NAME",true),
                new Field("NAME","Название",FieldType.TEXT),
                new Field("LEADER_ID","Командир",FieldType.REFERENCE,true,false,"Personnel","ID","FULL_NAME"),
                new Field("TYPE_ID","Тип",FieldType.REFERENCE,true,false,"Subdivision_Types","TYPE_ID","NAME")
        };

        }
    }
}
