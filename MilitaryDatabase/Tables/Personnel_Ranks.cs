﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Personnel_Ranks : Table
    {
        public Personnel_Ranks()
        {
            this.DbName = "Personnel_Ranks";
            this.Name = "Звания военнослужащих";
            this.isInsertable = false;
            this.isRemoveable = false;
            this.Fields = new List<Field> {
                new Field("RANK_ID", "Идентификатор звания",FieldType.NUMBER, false, true),
                new Field("NAME","Звание",FieldType.TEXT)
        };

        }
    }
}
