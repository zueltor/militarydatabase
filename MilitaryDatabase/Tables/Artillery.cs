﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Artillery : Table
    {
        public Artillery()
        {
            this.DbName = "Artillery";
            this.Name = "Артиллерия";
            this.Fields = new List<Field> {
                new Field("ID", "Артиллерия",FieldType.REFERENCE, false, true,"Armament","ID","NAME"),
                new Field("RELOAD_TIME","Время перезарядки",FieldType.NUMBER)
        };

        }
    }
}
