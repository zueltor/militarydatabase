﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Subdivision_Types : Table
    {
        public Subdivision_Types()
        {
            this.DbName = "Subdivision_Types";
            this.Name = "Типы подразделений";
            this.isInsertable = false;
            this.isRemoveable = false;
            this.Fields = new List<Field> {
                new Field("TYPE_ID", "Идентификатор типа",FieldType.NUMBER, false, true),
                new Field("NAME","Название",FieldType.TEXT)
        };

        }
    }
}
