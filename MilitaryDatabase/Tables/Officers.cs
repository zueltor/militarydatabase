﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Officers : Table
    {
        public Officers()
        {
            this.DbName = "Officers";
            this.Name = "Офицеры";
            this.Fields = new List<Field> {
                new Field("ID", "ФИО",FieldType.REFERENCE, false, true,"Personnel","ID","FULL_NAME"),
                new Field("GRADUATION_DATE", "Дата найма", FieldType.DATE),
        };

        }
    }
}
