﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Tractor : Table
    {
        public Tractor()
        {
            this.DbName = "Tractor";
            this.Name = "Тягачи";
            this.Fields = new List<Field> {
                new Field("ID", "Тягач",FieldType.REFERENCE, false, true,"Vehicle","ID","NAME"),
                new Field("TRACTION_CLASS","Тяговый класс",FieldType.NUMBER)
        };

        }
    }
}
