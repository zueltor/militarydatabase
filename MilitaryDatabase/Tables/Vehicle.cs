﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Vehicle : Table
    {
        public Vehicle()
        {
            this.DbName = "Vehicle";
            this.Name = "Техника";
            this.Fields = new List<Field> {
                new Field("ID", "Идентификатор техники",FieldType.NUMBER, false, true),
                new Field("NAME","Название техники",FieldType.TEXT)
        };

        }
    }
}
