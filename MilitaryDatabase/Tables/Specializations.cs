﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Specializations : Table
    {
        public Specializations()
        {
            this.DbName = "Specializations";
            this.Name = "Специализации";
            this.Fields = new List<Field> {
                new Field("ID", "Идентификатор специализации",FieldType.NUMBER, false, true),
                new Field("NAME","Специализация",FieldType.TEXT)
        };

        }
    }
}
