﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase
{
    public class Table
    {
        public string DbName { get; set; }
        public string Name { get; set; }
        public List<Field> Fields;
        public bool isInsertable = true;
        public bool isEditable = true;
        public bool isRemoveable = true;
        public bool SpecialInsert { get; set; }
        public string InsertProcedureName { get; set; }

    }
}
