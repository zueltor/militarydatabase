﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Personnel_Specialization : Table
    {
        public Personnel_Specialization()
        {
            this.DbName = "Personnel_Specialization";
            this.Name = "Специализации военнослужащих";
            this.isEditable = false;
            this.Fields = new List<Field> {
                new Field("PERSONNEL_ID", "ФИО",FieldType.REFERENCE, false, true,"Personnel","ID","FULL_NAME"),
                new Field("SPECIALIZATION_ID", "Специализация",FieldType.REFERENCE, false, true,"Specializations","ID","NAME")
        };

        }
    }
}
