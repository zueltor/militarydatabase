﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Constructions : Table
    {
        public Constructions()
        {
            this.DbName = "Constructions";
            this.Name = "Сооружения";
            this.Fields = new List<Field> {
                new Field("ID", "Идентификатор сооружения",FieldType.NUMBER, false, true),
                new Field("NAME","Сооружение",FieldType.TEXT),
                new Field("DISLOCATION_ID", "Идентификатор дислокации",FieldType.REFERENCE, false, true,"Dislocations","ID","City"),
                new Field("MU_ID", "Военная часть",FieldType.REFERENCE, false, true,"Subdivisions","ID","NAME",false,"TYPE_ID = 4")
        };

        }
    }
}

