﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Soldiers : Table
    {
        public Soldiers()
        {
            this.DbName = "Soldiers";
            this.Name = "Солдаты";
            this.Fields = new List<Field> {
                new Field("ID", "ФИО",FieldType.REFERENCE, false, true,"Personnel","ID","FULL_NAME"),
                new Field("SQUAD_ID", "Отделение", FieldType.REFERENCE,true, false, "Subdivisions", "ID", "NAME",true,"TYPE_ID = 1"),
        };

        }
    }
}
