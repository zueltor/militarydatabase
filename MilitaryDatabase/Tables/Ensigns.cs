﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Ensigns : Table
    {
        public Ensigns()
        {
            this.DbName = "Ensigns";
            this.Name = "Прапорщики";
            this.Fields = new List<Field> {
                new Field("ID", "ФИО",FieldType.REFERENCE, false, true,"Personnel","ID","FULL_NAME"),
                new Field("HIRE_DATE", "Дата найма", FieldType.DATE),
        };

        }
    }
}
