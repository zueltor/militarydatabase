﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class IFV : Table
    {
        public IFV()
        {
            this.DbName = "IFV";
            this.Name = "БМП";
            this.Fields = new List<Field> {
                new Field("ID", "БМП",FieldType.REFERENCE, false, true,"Vehicle","ID","NAME"),
                new Field("WEIGHT","Вес",FieldType.NUMBER)
        };

        }
    }
}
