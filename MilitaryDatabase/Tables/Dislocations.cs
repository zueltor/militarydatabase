﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Dislocations : Table
    {
        public Dislocations()
        {
            this.DbName = "Dislocations";
            this.Name = "Дислокации";
            this.Fields = new List<Field> {
                new Field("ID", "Идентификатор дислокации",FieldType.NUMBER, false, true),
                new Field("CITY","Город",FieldType.TEXT)
        };

        }
    }
}
