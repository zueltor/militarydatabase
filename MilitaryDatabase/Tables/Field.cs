﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MilitaryDatabase.Tables;

namespace MilitaryDatabase
{
    public class Field
    {
        public string DbName;
        public string Name;
        public bool IsKey;
        public bool IsEditable;
        public bool IsNullable;
        public string ReferenceTable;
        public string ReferenceName;
        public string DisplayName;
        public FieldType type;
        public bool special = false;
        public string Where;
        public Field(string DbName, string Name, FieldType type,bool IsEditable=true,bool IsKey = false,string ReferenceTable=null, string ReferenceName=null, string DisplayName=null,bool IsNullable=false, string Where=null)
        {
            this.DbName = DbName;
            this.Name = Name;
            this.type = type;
            this.IsEditable = IsEditable;
            this.IsKey = IsKey;
            this.ReferenceTable = ReferenceTable;
            this.ReferenceName = ReferenceName;
            this.DisplayName = DisplayName;
            this.IsNullable = IsNullable;
            this.Where = Where;
        }
    }


}
