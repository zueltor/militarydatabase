﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class Personnel : Table
    {
        public Personnel()
        {
            this.DbName = "Personnel";
            this.Name = "Военнослужащие";
            this.SpecialInsert = true;
            this.InsertProcedureName = "insert_personnel";
            this.Fields = new List<Field>
            {
                new Field("ID","Идентификатор",FieldType.NUMBER,false,true),
                new Field("RANK_ID","Звание",FieldType.REFERENCE, true,false,"Personnel_Ranks","RANK_ID","NAME"),
                new Field("FULL_NAME","ФИО",FieldType.TEXT),
                new Field("BIRTH_DATE","Дата рождения",FieldType.DATE),
                new Field("MULTI_DATE","Дата найма/окончания академии",FieldType.DATE),
                new Field("SQUAD_ID","Отделение",FieldType.REFERENCE,true,false,"Subdivisions","ID","NAME",false,"TYPE_ID = 1")
            };
            this.Fields[4].special = true;
            this.Fields[5].special = true;
        }
    }
}
