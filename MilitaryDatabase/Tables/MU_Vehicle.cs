﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilitaryDatabase.Tables
{
    class MU_Vehicle : Table
    {
        public MU_Vehicle()
        {
            this.DbName = "MU_Vehicle";
            this.Name = "Техника военных частей";
            this.Fields = new List<Field> {
                new Field("VEHICLE_ID", "Техника",FieldType.REFERENCE, false, true,"Vehicle","ID","NAME"),
                new Field("MU_ID", "Военная часть",FieldType.REFERENCE, false, true,"Subdivisions","ID","NAME",false,"TYPE_ID = 4"),
                new Field("AMOUNT","Количество",FieldType.NUMBER)
        };

        }
    }
}
