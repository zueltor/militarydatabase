﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MilitaryDatabase.Queries;
using MilitaryDatabase.Tables;
using Oracle.ManagedDataAccess.Client;

namespace MilitaryDatabase
{
    public partial class Form1 : Form
    {
        private List<Table> tables = new List<Table> { new Subdivisions(), new Dislocations(), new Constructions(), new Subdivision_Types(), new Specializations(), new Personnel(),
            new Personnel_Ranks(),new Personnel_Specialization(), new Soldiers(),new Sergeants(), new Ensigns(), new Officers(),new Vehicle(),
            new Tractor(),new IFV(), new Armament(), new Carbine(), new Artillery(), new MU_Armament(), new MU_Vehicle(), new Allowed_leaders() };
        private List<CheckBox> queryCheckBoxes = new List<CheckBox>();
        private List<Query> queries = new List<Query> { new Query1(), new Query2(), new Query3(), new Query4(),new Query5(),new Query6(),new Query7_1(),
        new Query7_2(),new Query8_1(),new Query8_2(),
        new Query9(), new Query10(), new Query11(), new Query12_1(),new Query12_2(),new Query13_1(),new Query13_2()
        };

        private Query selectedQuery = new Query1();
        BindingSource bindingTablesSource = new BindingSource();
        BindingSource bindingQueriesSource = new BindingSource();
        private OracleDataAdapter dataAdapter = new OracleDataAdapter();
        string connectionString;
        private BindingSource bindingSource1 = new BindingSource();
        private BindingSource bindingSource2 = new BindingSource();
        Table selectedTable = new Subdivisions();
        OracleConnection conn = new OracleConnection();
        public Form1(string connectionString)
        {
            this.connectionString = connectionString;
            this.conn.ConnectionString = connectionString;
            this.conn.Open();
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.bindingQueriesSource.DataSource = queries;
            this.comboBox2.DataSource = bindingQueriesSource.DataSource;
            this.comboBox2.DisplayMember = "Name";
            this.comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
            this.bindingTablesSource.DataSource = tables;
            this.comboBox1.DataSource = bindingTablesSource.DataSource;
            this.comboBox1.DisplayMember = "Name";
            this.comboBox1.ValueMember = "DbName";
            this.comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            //this.panel1.dataGridView1.DataSource = bindingSource1;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.DataSource = bindingSource1;
            this.dataGridView1.DataSource = bindingSource2;
            this.changeData();
            this.changeData2();
            this.Text = "Информационная система военного округа: Таблицы";
            this.panel1.Visible = true;
            this.panel2.Visible = false;


            //GetData("select * from subdivisions");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //conn.ConnectionString = connectionString;
            //conn.Open();
        }

        public void UpdateData()
        {
            GetData(dataAdapter.SelectCommand.CommandText);
        }



        private void GetData(string selectCommand)
        {
            try
            {
                dataAdapter = new OracleDataAdapter(selectCommand, connectionString);

                OracleCommandBuilder commandBuilder = new OracleCommandBuilder(dataAdapter);
                SetGridColumns();
                DataTable table = new DataTable();

                dataAdapter.Fill(table);

                bindingSource1.DataSource = table;
            }
            catch (OracleException ex)
            {
                foreach (OracleError error in ex.Errors)
                {
                    MessageBox.Show(error.Message, "Error");
                }
            }
        }

        private void GetData2()
        {
            try
            {
                string sql = selectedQuery.sql();
                if (sql == null)
                {
                    sql = "SELECT 'Empty' FROM DUAL WHERE 0 = 1";
                }
                OracleCommand cmd = new OracleCommand(sql, conn);
                for (int i = 0; i < selectedQuery.parameters.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)tableLayoutPanel1.Controls[i * 3];
                    if (checkBox.Checked)
                    {
                        cmd.BindByName = true;
                        string value;
                        Control control = tableLayoutPanel1.Controls[i * 3 + 2];
                        if (control is ComboBox)
                        {
                            ComboBox comboBox = (ComboBox)control;
                            value = comboBox.SelectedValue.ToString();
                        }
                        else
                        {
                            NumericUpDown numeric = (NumericUpDown)control;
                            value = numeric.Value.ToString();
                        }
                        cmd.Parameters.Add(selectedQuery.parameters[i].Name, value);
                    }
                }
                var dataAdapter2 = new OracleDataAdapter(cmd);

                OracleCommandBuilder commandBuilder = new OracleCommandBuilder(dataAdapter2);
                //SetGridColumns();
                DataTable table = new DataTable();

                dataAdapter2.Fill(table);

                bindingSource2.DataSource = table;
            }
            catch (OracleException ex)
            {
                foreach (OracleError error in ex.Errors)
                {
                    MessageBox.Show(error.Message, "Error");
                }
            }
        }

        private void SetGridColumns()
        {
            this.dataGridView2.Columns.Clear();
            foreach (var field in selectedTable.Fields)
            {
                if (field.special)
                {
                    continue;
                }
                if (field.ReferenceTable != null)
                {
                    DataTable comboTable = new DataTable();
                    string query = string.Format("SELECT {0}, {1} FROM {2}", field.ReferenceName, field.DisplayName, field.ReferenceTable);
                    OracleDataAdapter comboAdapter = new OracleDataAdapter(query, connectionString);
                    comboAdapter.Fill(comboTable);
                    object[] empty = { 0, "" };
                    comboTable.Rows.Add(empty);
                    DataGridViewComboBoxColumn combo = new DataGridViewComboBoxColumn
                    {
                        DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing,
                        HeaderText = field.Name,
                        Name = field.DisplayName,
                        DataPropertyName = field.DbName,
                        DataSource = comboTable,
                        DisplayMember = field.DisplayName,
                        ValueMember = field.ReferenceName
                    };
                    if (!field.IsEditable)
                    {
                        combo.ReadOnly = true;
                    }
                    this.dataGridView2.Columns.Add(combo);
                }
                else
                {
                    DataGridViewTextBoxColumn col = new DataGridViewTextBoxColumn();
                    col.HeaderText = field.Name;
                    col.Name = field.DisplayName;
                    col.DataPropertyName = field.DbName;
                    if (!field.IsEditable)
                    {
                        col.ReadOnly = true;
                    }
                    this.dataGridView2.Columns.Add(col);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataAdapter.Update((DataTable)bindingSource1.DataSource);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            GetData(dataAdapter.SelectCommand.CommandText);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //GetData("SELECT * FROM " + this.textBox1.Text);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //UpdateEditPanel();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form2 form = new Form2(this, connectionString, selectedTable, dataGridView2.SelectedRows[0]);
            form.Visible = true;
            this.Enabled = false;

        }

        private void button6_Click(object sender, EventArgs e)
        {
            Form2 form = new Form2(this, connectionString, selectedTable);
            form.Visible = true;
            this.Enabled = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Вы точно хотите удалить запись?",
                                     "Подтвердите удаление",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                string query = "DELETE FROM " + selectedTable.DbName + " WHERE ";
                int keyParameters = 0;
                for (int i = 0; i < selectedTable.Fields.Count; i++)
                {
                    if (selectedTable.Fields[i].IsKey)
                    {
                        if (keyParameters > 0)
                        {
                            query += " AND ";
                        }
                        query += selectedTable.Fields[i].DbName + "=" + dataGridView2.SelectedRows[0].Cells[i].Value;
                    }
                }

                Console.WriteLine(query);
                using (OracleConnection conn = new OracleConnection())
                {
                    conn.ConnectionString = connectionString;
                    conn.Open();
                    OracleCommand command = conn.CreateCommand();
                    command.CommandText = query;
                    try
                    {
                        var a = command.ExecuteReader();
                    }
                    catch (OracleException ex)
                    {
                        foreach (OracleError error in ex.Errors)
                        {
                            MessageBox.Show(error.Message, "Error");
                        }
                        return;
                    }
                    //Console.WriteLine("Affected " + a + " rows");


                }

                UpdateData();

                //SQL sql = new SQL(connectionString);
                //sql.executeQuery(query);
                //dataGridView2.Rows.Remove(dataGridView2.SelectedRows[0]);
            }

        }

        private void changeData()
        {
            //this.comboBox1.SelectedIndex = 0;
            this.selectedTable = (Table)this.comboBox1.SelectedItem;
            this.editButton.Enabled = this.selectedTable.isEditable;
            this.insertButton.Enabled = this.selectedTable.isInsertable;
            this.removeButton.Enabled = this.selectedTable.isRemoveable;
            GetData("SELECT * FROM " + this.selectedTable.DbName);
        }

        private void changeData2()
        {
            this.tableLayoutPanel1.Controls.Clear();
            this.selectedQuery = (Query)this.comboBox2.SelectedItem;
            this.selectedQuery.reset();
            this.queryCheckBoxes.Clear();
            var controls = new List<Control>();
            Control control = new Control();
            for (int i = 0; i < selectedQuery.parameters.Count; i++)
            {
                var param = selectedQuery.parameters[i];
                var label = new Label()
                {
                    Text = param.Label,
                    //AutoSize = false,
                    TextAlign = ContentAlignment.MiddleLeft,
                    Dock = DockStyle.Fill
                };
                var checkBox = new CheckBox()
                {
                    Dock = DockStyle.Fill,
                    CheckAlign = ContentAlignment.MiddleCenter
                };
                this.queryCheckBoxes.Add(checkBox);
                this.tableLayoutPanel1.Controls.Add(checkBox, 0, i);
                this.tableLayoutPanel1.Controls.Add(label, 1, i);
                if (param.Type == ParameterType.REFERENCE)
                {
                    DataTable comboTable = new DataTable();
                    string query = param.ParameterSQL;
                    OracleDataAdapter comboAdapter = new OracleDataAdapter(query, connectionString);
                    comboAdapter.Fill(comboTable);
                    object[] empty = { 0, "" };
                    comboTable.Rows.Add(empty);

                    ComboBox comboBox = new ComboBox
                    {
                        DataSource = comboTable,
                        DisplayMember = param.DisplayName,
                        ValueMember = param.ValueName,
                        Dock = DockStyle.Fill,
                        DropDownStyle = ComboBoxStyle.DropDownList
                    };
                    control = comboBox;
                    comboBox.SelectedIndexChanged += (sender, e) => { GetData2(); };

                }
                else
                {
                    NumericUpDown numeric = new NumericUpDown();
                    numeric.Maximum = int.MaxValue;
                    numeric.Minimum = 0;
                    control = numeric;
                }
                control.Enabled = false;
                this.tableLayoutPanel1.Controls.Add(control, 2, i);
                controls.Add(control);

            }
            if (queryCheckBoxes.Count > 0)
            {
                queryCheckBoxes[0].CheckedChanged += (csender, ce) =>
                {
                    CheckBox checkBox = (CheckBox)csender;
                    if (checkBox.Checked)
                    {
                        selectedQuery.index++;
                        controls[0].Enabled = true;
                    }
                    else
                    {
                        selectedQuery.index--;
                        controls[0].Enabled = false;
                    }
                    GetData2();
                };
            }
            if (queryCheckBoxes.Count > 1)
            {
                queryCheckBoxes[1].CheckedChanged += (csender, ce) => { checkChanged(csender, controls[1], queryCheckBoxes[0]); };
            }
            GetData2();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBox1.Focused)
            {
                changeData();
                /*this.selectedTable = (Table)this.comboBox1.SelectedItem;
                GetData("SELECT * FROM " + this.selectedTable.DbName);*/
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            this.Text = "Информационная система военного округа: Таблицы";
            this.panel1.Visible = true;
            this.panel2.Visible = false;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.Text = "Информационная система военного округа: Запросы";
            this.panel1.Visible = false;
            this.panel2.Visible = true;
        }

        void checkChanged(object sender, Control control, CheckBox otherCheckBox)
        {
            CheckBox checkBox = (CheckBox)sender;
            if (checkBox.Checked)
            {
                otherCheckBox.Checked = true;
                otherCheckBox.Enabled = false;
                control.Enabled = true;
                selectedQuery.index++;
            }
            else
            {
                otherCheckBox.Enabled = true;
                selectedQuery.index--;
                control.Enabled = false;
            }
            GetData2();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBox2.Focused)
            {
                changeData2();
            }
        }
    }
}
