﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;

namespace MilitaryDatabase
{
    public partial class Form3 : Form
    {
        private bool exitApplication = true;
        public Form3()
        {
            
            InitializeComponent();
            this.Text = "Информационная система военного округа";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string connectionString = string.Format("Data Source=(DESCRIPTION =" + "(ADDRESS = (PROTOCOL = TCP)(HOST ={0})(PORT = 1521))" + "(CONNECT_DATA =" + "(SERVER = DEDICATED)" + "(SERVICE_NAME = XE)));" + "User Id={1};Password={2};",
                this.textBox1.Text, this.textBox2.Text, this.textBox3.Text);
            OracleConnection myConnection = new OracleConnection();
            myConnection.ConnectionString = connectionString;
            try
            {
                myConnection.Open();
            }
            catch (OracleException ex)
            {
                foreach (OracleError error in ex.Errors)
                {
                    MessageBox.Show(error.Message, "Error");
                }
                return;
            }
            finally
            {
                myConnection.Close();
            }

            this.exitApplication = false;
            this.Close();
            var databaseForm = new Form1(connectionString);
            databaseForm.Show();
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.textBox1.Text = "localhost";
            this.textBox2.Text = "chris";
            this.textBox3.Text = "1234";
        }

        private void Form3_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (exitApplication)
            {
                Application.Exit();
            }
        }
    }
}
